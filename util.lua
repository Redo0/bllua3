R"( do

-- This Lua code provides some built-in utilities for writing Lua add-ons
-- It is eval'd automatically once BLLua3 has loaded the TS API and environment
-- It only has access to the sandboxed lua environment, just like user code.

-- Implement print using ts.echo so it prints to BL console
print = function(...)
	local t = {...}; local o = {};
	for i = 1, #t do o[i] = tostring(t[i]) end
	ts.echo(table.concat(o, "    "))
end

-- Provide limited OS functions
os = {
	time = function() return math.floor(tonumber(ts.call("getSimTime"))/1000) end,
	clock = function() return tonumber(ts.call("getSimTime"))/1000 end,
}

-- schedule: Use TS's schedule function to schedule lua calls
ts._schedule_table  = ts._schedule_table or {}
ts._schedule_by_obj = ts._schedule_by_obj or {}
ts._schedule_nextid = ts._schedule_nextid or 1
function schedule(time, func, ...)
	-- get a unique schedule ID
	local sched_id = ts._schedule_nextid
	ts._schedule_nextid = ts._schedule_nextid+1
	-- create a callback to call the function when the schedule ends
	local fargs = {...}
	local callback = function()
		func(unpack(fargs))
	end
	-- put it in the schedule table
	ts._schedule_table[sched_id] = { [1] = callback }
	-- use ts schedule to schedule the event
	local sched_obj = tonumber(ts.call("schedule", time, 0, "luacall", "_ts_schedule_callback", sched_id))
	-- save the schedule reference in a table and return the id
	ts._schedule_table[sched_id][2] = sched_obj
	return sched_id
end
function _ts_schedule_callback(sched_id)
	-- get schedule out of schedule table by id
	sched_id = tonumber(sched_id)
	local sched = ts._schedule_table[sched_id]
	assert(sched, "_ts_schedule_callback: no schedule with id "..sched_id)
	ts._schedule_table[sched_id] = nil
	-- call it
	sched[1]()
end
function cancel(sched_id_s)
	sched_id = tonumber(sched_id_s)
	-- get the schedule by id out of the table
	local sched = ts._schedule_table[sched_id]
	if not sched then return end
	-- cancel it in ts
	ts.call("cancel", sched[2])
	-- remove from table
	ts._schedule_table[sched_id] = nil
end

-- wrap io.open to allow reading from zips
-- Not perfect because TS file I/O sucks - Can't read nulls, can't distinguish between CRLF and LF.
local old_io_open = io.open
io.open = function(fn, mode)
	-- use original mode if possible
	local res = old_io_open(fn, mode)
	if res then return res end
	-- otherwise fall back to TS mode
	local exist = ts.call("isFile", fn)
	if not exist then return nil end
	-- if TS sees file but Lua doesn't, it must be in a zip, so use TS reader
	local data = ts.call("_bllua3_ReadEntireFile", fn)
	-- return a temp lua file object with the data
	local fi = io.tmpfile()
	fi:write(data)
	fi:seek("set")
	return fi
end

-- provide io.lines
function io.lines(fn)
	local fi = io.open(fn)
	if not fi then error("Could not find file "..fn, 2) end
	return fi:lines()
end

-- provide eval
function eval(code)
	return assert(loadstring(code))()
end

-- provide dofile and require
function dofile(fn, n)
	local fi = io.open(fn)
	if not fi then error("Could not find file "..fn, n and n+2 or 2) end
	local text = fi:read("*a")
	fi:close()
	return eval(text)
end
function require(fn)
	if fn=="ffi" or fn=="socket" then return requiresecure(fn)
	else return dofile("luainc/"..fn..".lua", 1)
	end
end

-- get and set globals
function ts.get(name) return ts.call("_bllua_get_var", name) end
function ts.set(name, val) ts.call("_bllua_set_var", name, val) end
function ts.getobj(obj, name) return ts.call("_bllua_get_var_obj", obj, name) end
function ts.setobj(obj, name, val) ts.call("_bllua_set_var_obj", obj, name, val) end

function _bllua_get_var(name) return _G[name] end
function _bllua_set_var(name, val) _G[name] = val end

-- ts.eval and ts.exec
function ts.eval(code) return ts.call("eval", code) end
function ts.exec(file) return ts.call("exec", file) end

-- packages
--ts._package_table = ts._package_table or {}
--function _bllua_call_package(id, ...)
--	id = tonumber(id)
--	local func = ts._package_table[id] or error("no function with id "..id)
--	--local parent = 
--	return func(parent, ...)
--end
--function ts.package(pname, fname, func)
--	local arglist = [[%a, %b, %c, %d, %e, %f, %g, %h, %i, %j, %k, %l, %m, %n, %o, %p, %q, %r, %s, %t]]
--	ts.eval [[
--		package ]]..pname..[[ {
--			function ]]..fname..[[ (]]..arglist..[[) {
--				return luacall(_bllua_call_package, ]]..func_id_pre..[[, ]]..arglist..[[);
--			}
--		};
--	]]
--end

print("    Lua util loaded")

end )"