R"( do

-- This Lua code sets up the "environment" for sandboxed game code to run in.
--   It deletes all global variables except a whitelist, and implements some functionality
--   with safety wrappers.
-- This code is always executed first in the Lua environment;
--   It is included into bllua3.cpp at compile-time, hardcoded in bllua3.dll,
--   and eval'd during initialization, before the environment is exposed to any user code.
--   Because of this, it is impossible for user code to access unsafe functionality
--   except through the functions provided here.

-- Utility: Convert a list of strings into a map of string->true
local tflip = function(t) local u = {}; for _, n in ipairs(t) do u[n] = true end; return u; end

-- Save banned global variables for wrapping with safe functions
local old_dofile = dofile
local old_io = io
local old_require = require

-- Remove all global variables except a whitelist
local ok_names = tflip {
	"_G",
	"ts",
	"string", "table", "math", "coroutine",
	"unpack", "pairs", "ipairs",
	"error", "assert",
	"type", "tostring", "tonumber",
	"loadstring",
	"getmetatable", "setmetatable", "rawget", "rawset"
}
local names = {}
for n, _ in pairs(_G) do if not ok_names[n] then table.insert(names, n) end end
for _, n in ipairs(names) do _G[n] = nil end

-- Sanitize a file path to point only to allowed files within the blockland directory
local safe_dirs = tflip{"add-ons", "base", "config", "saves", "screenshots", "shaders"}
local safe_dirs_readonly = tflip{"luainc"}
local safe_exts = tflip{"txt", "bls", "blb", "png", "jpg", "ogg", "wav", "dts", "dsq", "gui", "cs", "lua", "dat", "log", "glsl", "html", "zip", "dso", "out"}
local function safe_path(fn, readonly)
	fn = fn:gsub("\\", "/")
	fn = fn:gsub("^ +", "")
	fn = fn:gsub(" +$", "")
	local ic = fn:find("[^a-zA-Z0-9_%-/ %.]"); if ic then error("filename contains invalid character "..fn:sub(ic, ic).." at position "..ic, 2) end
	if fn:find("^%.") or fn:find("%.%.") or fn:find(":") or fn:find("^/") then error("filename contains invalid sequence", 2) end
	local dir = fn:match("^([^/]+)/")
	if (not dir) or (
		(not safe_dirs[dir:lower()]) and
		((not readonly) or (not safe_dirs_readonly[dir:lower()]))
	) then error("filename is in disallowed directory "..(dir or "nil"), 2) end
	local ext = fn:match("%.([^/%.]+)$"); if (not ext) or (not safe_exts[ext:lower()]) then error("filename has disallowed extension "  ..(ext or "nil"), 2) end
	return fn
end

-- Provide limited I/O using safe paths
io = {
	open  = function(fn, md)
		md = md or "r"
		local readonly = md=="r"
		local fns = safe_path(fn, readonly)
		return old_io.open(fns, md)
	end,
	type  = old_io.type,
	tmpfile = old_io.tmpfile,
}

-- Allow requiring certain external packages
function requiresecure(mod)
	if mod=="socket" or mod=="ffi" then
		return old_require(mod)
	else
		error("invalid module "..mod)
	end
end

ts.echo("    Lua env loaded")

end )"